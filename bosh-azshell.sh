# Install bosh from Azure Shell
# in azure shell run pip install certbot
# change quota of Azure shell file share

az ad sp create-for-rbac -n bosh -o json > sp.json
az role assignment create --assignee $(jq -r .appId sp.json) --role "Contributor" --scope /subscriptions/$(az account list --query "[?isDefault].id" -o tsv)

SP_FILE="$HOME/pre/sp.json"
export NAME=${PWD##*/}

export SUBSCRIPTION_ID=$(az account list --query "[?isDefault].id" -o tsv)
export TENANT_ID=$(az account list --query "[?isDefault].tenantId" -o tsv)
export CLIENT_ID=$(jq -r .appId ${SP_FILE})
export CLIENT_SECRET=$(jq -r .password ${SP_FILE})
export REGION="westeurope"
export DOMAIN="withazure.dev"
export BBL_STATE_DIR=$(pwd)/state
export BBL_AZURE_SUBSCRIPTION_ID=$SUBSCRIPTION_ID
export BBL_AZURE_CLIENT_ID=$CLIENT_ID
export BBL_AZURE_CLIENT_SECRET=$CLIENT_SECRET
export BBL_AZURE_TENANT_ID=$TENANT_ID

certbot certonly  --manual --preferred-challenge=dns --email=alessandro.vozza@outlook.com \
--server https://acme-v02.api.letsencrypt.org/directory --manual-public-ip-logging-ok \
--agree-tos -d "cf.${DOMAIN}" \
--config-dir ./certs/ \
--work-dir ./certs/ \
--logs-dir ./certs/

#on another window
az network dns record-set txt delete -g dns -z withazure.dev -n _acme-challenge -y
az network dns record-set txt add-record \
-g dns -z "withazure.dev" -n _acme-challenge.cf \
--value "wY6ItEjz14J-0j_rc38UXrI8gPYqKwjII8NXKCcJjuY"

# https://github.com/cloudfoundry/bosh-bootloader/issues/256
KEY_DOMAIN_NAME="../pre/certs/live/cf.withazure.dev/"
KEY_PASSWORD=$(openssl rand -base64 32)
openssl pkcs12 -export -out PFX_FILE \
-certpbe PBE-SHA1-3DES -keypbe PBE-SHA1-3DES -macalg sha1 \
-inkey $KEY_DOMAIN_NAME/privkey.pem -in $KEY_DOMAIN_NAME/fullchain.pem -password pass:$KEY_PASSWORD
echo $KEY_PASSWORD > PFX_FILE_PASS

bbl plan \
  --iaas azure \
  --name $NAME \
  --azure-subscription-id ${SUBSCRIPTION_ID} \
  --azure-tenant-id ${TENANT_ID} \
  --azure-client-id ${CLIENT_ID} \
  --azure-client-secret ${CLIENT_SECRET} \
  --azure-region ${REGION} \
  --lb-type cf \
  --lb-cert PFX_FILE \
  --lb-key PFX_FILE_PASS \
  --lb-domain cf.${DOMAIN} 


CUSTOM_CIDR="192.168.100.0/24"
DOMAIN_NAME="cf.${DOMAIN}"
echo "system_domain=$DOMAIN_NAME" >> ./vars/bbl.tfvars
echo "network_cidr=$CUSTOM_CIDR" >> ./vars/bbl.tfvars
echo "internal_cidr=$CUSTOM_CIDR" >> ./vars/bbl.tfvars


#change SKU of app gw in terraform/bbl-template.cf to Standard_v2
bbl up --iaas azure \
  --name $NAME \
  --azure-subscription-id ${SUBSCRIPTION_ID} \
  --azure-tenant-id ${TENANT_ID} \
  --azure-client-id ${CLIENT_ID} \
  --azure-client-secret ${CLIENT_SECRET} \
  --azure-region ${REGION} \
  --lb-type cf \
  --lb-cert PFX_FILE \
  --lb-key PFX_FILE_PASS \
  --lb-domain cf.${DOMAIN} \
  --debug


export BBL_IAAS=azure
export BBL_AZURE_CLIENT_ID=${CLIENT_ID} 
export BBL_AZURE_CLIENT_SECRET=${CLIENT_SECRET}
export BBL_AZURE_REGION=${REGION} 
export BBL_AZURE_SUBSCRIPTION_ID=${SUBSCRIPTION_ID}
export BBL_AZURE_TENANT_ID=${TENANT_ID}

bbl up

eval "$(bbl print-env)"
bosh alias-env azure 
bosh -e azure login

bosh -n update-cloud-config ~/cf1/bosh-deployment/azure/cloud-config.yml \
  -v internal_cidr=10.0.0.0/16 \
  -v internal_gw=10.0.0.1 \
  -v vnet_name=cf1-bosh-vn \
  -v subnet_name=cf1-bosh-sn \
  -v security_group=cf1-cf \
  -v application_gateway=cf1-app-gateway

bosh update-runtime-config bosh-deployment/runtime-configs/dns.yml --name dns

bosh upload-stemcell --sha1 734a864a24f04ef891dd773e07891a9aa5dd6b17 \
  https://bosh.io/d/stemcells/bosh-azure-hyperv-ubuntu-jammy-go_agent?v=1.93

bosh -n -e azure -d cf deploy ~/cf1/cf-deployment/cf-deployment.yml \
-v system_domain=${DOMAIN} \
-o ~/cf1/cf-deployment/operations/scale-to-one-az.yml \
-o ~/cf1/cf-deployment/operations/azure.yml \
-o ~/cf1/cf-deployment/operations/use-compiled-releases.yml \
-o ~/cf1/cf-deployment/operations/enable-nfs-volume-service.yml \
--vars-store vars/cloud-config-vars.yml